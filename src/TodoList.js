import React from "react";
import uuid from "uuid";
import Button from "@material-ui/core/Button";
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import DeleteRounded from "@material-ui/icons/DeleteRounded";
import TextField from "@material-ui/core/TextField";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Checkbox from "@material-ui/core/Checkbox";
import Input from "@material-ui/core/Input";

class TodoList extends React.Component {
  state = {
    showTodos: true,
    newTodoText: "",
    todos: [
      {
        id: uuid(),
        text: "todo 1",
        isActive: false
      },
      {
        id: uuid(),
        text: "todo 2",
        isActive: false
      }
    ],
    filterStatus: "All" // 'Active | 'Complited'
  };

  render() {
    console.log(JSON.stringify(this.state, null, 2));
    return (
      <div>
        <h1>TODO list</h1>
        <ExpandMoreIcon
          onClick={() => {
            this.setState({
              showTodos: !this.state.showTodos
            });
          }}
        />

        <TextField
          variant="outlined"
          value={this.state.newTodoText}
          onChange={e => {
            const value = e.target.value;
            console.log(value);
            this.setState({ newTodoText: value });
          }}
        />
        <Fab
          style={{
            background: "#970bde"
          }}
          aria-label="add"
          onClick={() => {
            const newText = this.state.newTodoText;
            if (newText === "") {
              alert("ALERT");
              return;
            }
            this.setState({
              todos: [
                {
                  id: uuid(),
                  text: newText,
                  isActive: false
                },
                ...this.state.todos
              ],
              newTodoText: ""
            });
          }}
        >
          <AddIcon />
        </Fab>
        <ul
          style={{
            display: this.state.showTodos ? "block" : "none",
            listStyleType: "none"
          }}
        >
          {this.state.todos
            .filter(todo => {
              if (this.state.filterStatus === "All") {
                return true;
              } else if (this.state.filterStatus === "Active") {
                return todo.isActive ? false : true;
              } else if (this.state.filterStatus === "Complited") {
                return todo.isActive ? true : false;
              }
            })
            .map((todo, index) => (
              <li key={todo.id}>
                <Checkbox
                  checked={todo.isActive}
                  color="primary"
                  onChange={e => {
                    const isActive = e.target.checked;
                    const updatedTodos = this.state.todos.map(stateTodo => {
                      if (stateTodo.id === todo.id) {
                        return {
                          ...stateTodo,
                          isActive: isActive
                        };
                      }
                      return stateTodo;
                    });
                    this.setState({
                      todos: updatedTodos
                    });
                  }}
                />
                <Input
                  type="text"
                  value={todo.text}
                  onChange={e => {
                    const newText = e.target.value;
                    console.log(newText);
                    this.setState({
                      todos: this.state.todos.map(todoToUpdate => {
                        if (todoToUpdate.id === todo.id) {
                          return {
                            ...todoToUpdate,
                            text: newText
                          };
                        } else {
                          return todoToUpdate;
                        }
                      })
                    });
                  }}
                />
                <Fab
                  color="secondary"
                  aria-label="DeleteRounded"
                  onClick={() => {
                    this.setState({
                      todos: this.state.todos.filter(
                        todoToUpdate => todoToUpdate.id !== todo.id
                      )
                    });
                  }}
                >
                  <DeleteRounded />
                </Fab>
              </li>
            ))}
        </ul>

        <div>
          <Button
            variant="contained"
            color={this.state.filterStatus === "All" ? "primary" : "default"}
            onClick={() => {
              this.setState({
                filterStatus: "All"
              });
            }}
          >
            All
          </Button>
          <Button
            variant="contained"
            color={this.state.filterStatus === "Active" ? "primary" : "default"}
            onClick={() => {
              this.setState({
                filterStatus: "Active"
              });
            }}
          >
            Active
          </Button>
          <Button
            variant="contained"
            color={
              this.state.filterStatus === "Complited" ? "primary" : "default"
            }
            onClick={() => {
              this.setState({
                filterStatus: "Complited"
              });
            }}
          >
            Complited
          </Button>
          <Button
            variant="contained"
            color="secondary"
            disabled={this.state.todos > [] ? false : true}
            onClick={() => {
              this.setState({
                todos: []
              });
            }}
          >
            Clear all
          </Button>
        </div>
        <div></div>
      </div>
    );
  }
}

export default TodoList;
